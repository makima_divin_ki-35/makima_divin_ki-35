﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Web;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web.Script.Serialization;

namespace InternetMagazine
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private User _user = new User();
        private RequestProvider _api = new RequestProvider();
        public bool isAdmin = false;

        public MainWindow(){
            InitializeComponent();
        }

        private void Button_Click_ReturnSignIn(object sender, RoutedEventArgs e){
            Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            SignUp.Height = new System.Windows.GridLength(100, GridUnitType.Star);
            Products.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            AddProduct.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
        }


        private void RegisterClick(object sender, RoutedEventArgs e) {
            bool flag = true;
            User user = new User();

            if(tbLogin.Text == ""){
                tbLogin.BorderBrush = Brushes.Red;
                flag = false;
            }else{ user._name = tbLogin.Text; }

            if(tbNameSur.Text == ""){
                tbNameSur.BorderBrush = Brushes.Red;
                flag = false;
            }else{ user._name_surname = tbNameSur.Text; }

            if (tbPass.Text == ""){
                tbPass.BorderBrush = Brushes.Red;
                flag = false;
            }
            else { user._password = tbPass.Text; }

            if (tbPhone.Text == ""){
                tbPhone.BorderBrush = Brushes.Red;
                flag = false;
            }
            else { user._phone_number = tbPhone.Text; }

            if (flag){
                _api.RgisterUser(user._name, user._password);

                SignUp.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
                Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
                Success.Height = new System.Windows.GridLength(100, GridUnitType.Star);
                Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
                Products.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            }
            
        }

        private void SignIn(object sender, RoutedEventArgs e) {
            
            _user._name = this.username.Text;
            _user._password = this.pass.Text;

            var resp = _api.Auth(this.username.Text, this.pass.Text);
            Console.WriteLine(resp);
            if ( resp == null)
            {
                username.BorderBrush = Brushes.Red;
                pass.BorderBrush = Brushes.Red;
                return; 
            }

            var jss = new JavaScriptSerializer();
            Dictionary<string, string> sData = jss.Deserialize<Dictionary<string, string>>(resp);

            SignUp.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Products.Height = new System.Windows.GridLength(100, GridUnitType.Star);
            _user._token = sData["token"];

            this.AddButton.IsEnabled = (sData["isAdmin"] == "True")? true : false;
            

            var categoryResp = _api.GetCategory(sData["token"]);
            categoryResp = categoryResp.Substring(0, categoryResp.Length - 1).Substring(20);

            List<Category> categoryList = jss.Deserialize<List<Category>>(categoryResp);

            this.Categories.Items.Clear();
            this.addCategory.Items.Clear();

            foreach (var cat in categoryList)
            {
                this.Categories.Items.Add(cat.title);
                this.addCategory.Items.Add(new KeyValuePair<string, string>(cat.title, cat.id));
            }
        }

        
        
        private void Button_Click_SignUp(object sender, RoutedEventArgs e){
            SignUp.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Registration.Height = new System.Windows.GridLength(100, GridUnitType.Star);
        }

        private void Categories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var jss = new JavaScriptSerializer();
            var prods = _api.GetProduct(this.Categories.SelectedIndex + 1);

            prods = prods.Substring(0, prods.Length - 1).Substring(20);

            List<Product> prodList = jss.Deserialize<List<Product>>(prods);

            this.ProductsList.Items.Clear();

            foreach (var prod in prodList)
            {
                this.ProductsList.Items.Add(new {
                    Title = "Title: " +prod.title,
                    Desc = "Description: " + prod.description,
                    Imgurl = prod.photourl,
                    Price = "Price: " + prod.price + "uah"
                });
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            SignUp.Height = new System.Windows.GridLength(100, GridUnitType.Star);
            Products.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            SignUp.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Products.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            AddProduct.Height = new System.Windows.GridLength(100, GridUnitType.Star);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _api.AddProduct(
                this.addTitle.Text,
                this.addDesc.Text,
                this.addUrl.Text,
                this.addPrice.Text,
                ((KeyValuePair<string, string>)this.addCategory.SelectedValue).Value
            );

            this.addTitle.Text = "";
            this.addDesc.Text = "";
            this.addUrl.Text = "";
            this.addPrice.Text = "";

            Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            SignUp.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Products.Height = new System.Windows.GridLength(100, GridUnitType.Star); 
            AddProduct.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Registration.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Info.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Success.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            SignUp.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
            Products.Height = new System.Windows.GridLength(100, GridUnitType.Star);
            AddProduct.Height = new System.Windows.GridLength(0, GridUnitType.Pixel);
        }
    }
}
