﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InternetMagazine
{
    class RequestProvider
    {
        private string url = "http://188.166.160.150:3009";

        public string Auth(string username, string password)
        {
            try
            {
                var baseAddress = this.url + "/authenticate";

                var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "POST";

                string parsedContent = "{\"name\":\"" + username + "\", \"password\": \"" + password + "\"}";
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes(parsedContent);

                Stream newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();

                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                return content;
            }catch(Exception e){
                return null;
            }
            
        }

        public string RgisterUser(string username, string password)
        {
            try
            {
                var baseAddress = this.url + "/user";

                var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "POST";

                string parsedContent = "{\"username\":\"" + username + "\", \"password\": \"" + password + "\"}";
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes(parsedContent);

                Stream newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();

                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                return content;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public string AddProduct(string title, string desription, string url, string price, string catid)
        {
            try
            {
                var baseAddress = this.url + "/products";

                var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "POST";

                string parsedContent = "{\"title\":\"" + title + 
                    "\", \"description\": \"" + desription + 
                    "\", \"photourl\":\""+ url + 
                    "\", \"price\":\""+ price + 
                    "\", \"categoryId\":\"" + catid + "\"}";
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes(parsedContent);

                Stream newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();

                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                return content;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public string GetCategory(string token)
        {
            try
            {
                var baseAddress = this.url + "/category?token=" + token;

                var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "GET";

                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                return content;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string GetProduct(int id){
            try{
                var baseAddress = this.url + "/products/" + id.ToString();
                var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "GET";

                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                return content;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
