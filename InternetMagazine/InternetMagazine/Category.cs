﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetMagazine
{
    public class Category
    {
        public string id;
        public string title;
        public List<Product> products;

        public override string ToString()
        {
            return id + ":" + title;
        }
    }

    public class Product
    {
        public string title { get; set; }
        public string description { get; set; }
        public string photourl { get; set; }
        public string price { get; set; }
    }
}
